# Grafana con prometheus para docker


## Configurar Docker

1. Crear el daemon.json file para Docker.

vim /etc/docker/daemon.json

```JSON
{
  "metrics-addr" : "0.0.0.0:9323",
  "experimental" : true
}
```

2. Reiniciar el servicio de docker.

`systemctl restart docker`

3. Instalar firewald

`sudo apt install -y firewalld`

4. Añadir el puerto `firewall-cmd --add-port 9090/tcp`


## Configurar prometheus

1. prometheus.yml

<PRIVATE_IP_ADDRESS> Private ip address de la instancia de docker

En Debian 10: `ip address` coger docker inet ip

```YML
scrape_configs:
  - job_name: prometheus
    scrape_interval: 5s
    static_configs:
    - targets:
      - prometheus:9090
      - node-exporter:9100
      - pushgateway:9091
      - cadvisor:8080

  - job_name: docker
    scrape_interval: 5s
    static_configs:
    - targets:
      - <PRIVATE_IP_ADDRESS>:9323
```

## Docker Compose

1. ~/docker-compose.yml

```YML
version: '3'
services:
  prometheus:
    image: prom/prometheus:latest
    container_name: prometheus
    ports:
      - 9090:9090
    command:
      - --config.file=/etc/prometheus/prometheus.yml
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml:ro
    depends_on:
      - cadvisor
  cadvisor:
    image: google/cadvisor:latest
    container_name: cadvisor
    ports:
      - 8080:8080
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
  pushgateway:
    image: prom/pushgateway
    container_name: pushgateway
    ports:
      - 9091:9091
  node-exporter:
    image: prom/node-exporter:latest
    container_name: node-exporter
    restart: unless-stopped
    expose:
      - 9100
  grafana:
    image: grafana/grafana
    container_name: grafana
    ports:
      - 3000:3000
    environment:
      - GF_SECURITY_ADMIN_PASSWORD=password
    depends_on:
      - prometheus
      - cadvisor
```

2. Aplicar los cambios.

`docker-compose up -d`

3. Asegurar que todos los contenedores estan levantados.

`docker ps`

4. Abrir el navegador en nuestra ip http://PUBLIC_IP_ADDRESS:9090.

- Click Status y ver los Targets.
Todo debe estar ok y funcionando.

## Grafana

1. Abrir Grafana http://PUBLIC_IP_ADDRESS:3000 en tu navegador.

Log in with admin password

2. Crear un datasource de grafana

3. Add "Prometheus".
  - en la URL, select http://localhost:9090. Usar la IP privada de la instancia de docker. Reemplazar "localhost" por ella: http://PRIVATE_IP_ADDRESS:9090).

4. Guardar y testear, todo debe funcionar bien

5. Importar el docker-dashboard.json para grafana seleccionando el dropdown menu de prometheus

(docker-dashboard.json)[https://raw.githubusercontent.com/linuxacademy/content-intermediate-docker-quest-prometheus/master/dashboards/docker_and_system.json]

### Add an Email Notification Channel

Click Add channel. Tip "Email". En Email addresses, añade tu email address. Guardar.

OJO: Si no tenemos un servidor smtp no enviará emails, pero nos vale para hacer la prueba


### Create an Alert for CPU Usage

1. Ir a nuestro dashboards

Click en el gráfico de CPU Usage, y editar Edit desde el dropdown.

2. Click Add Query y añade esto

`sum(rate(process_cpu_seconds_total[1m])) * 100`

Click en el Alert tab, y crea la alerta. En las condiciones de la query, selecciona E** del dropdown. y en IS ABOVE, escribe la regla. Ej:75.

3. Test Rule.

4. Click Notifications, click en + icon. Selecciona Email Alerts. Escribe un mensage del tipo "CPU usage is over 75%."

5. Guarda tu dashboar arriba de la pantalla.

Añade una descripción y guarda.
